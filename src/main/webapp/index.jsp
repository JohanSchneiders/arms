<%--
  Created by IntelliJ IDEA.
  User: jschneiders
  Date: 5-12-17
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>ARMS </title>
        <!-- stylesheets -->
        <!-- Bootstrap framework via CDN--><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
            <!-- own stylesheets -->
        <link rel="stylesheet" type="text/css" href="css/sideMenu.css"/>
        <link rel="stylesheet" type="text/css" href="css/index.css"/>
        <!-- javascript -->
        <!-- jquery --><script type="text/javascript" src="js/externalLibs/jquery.js"></script>
        <!-- popper --><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
        <!-- bootstrap js --><script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
        <!-- Plotly.js --><script type="text/javascript" src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        <!-- other scripts -->
        <script type="text/javascript" src="js/externalLibs/jquery.form.js"></script>
        <script type="text/javascript" src="js/alert.js"></script>
        <script type="text/javascript" src="js/rendering.js"></script>
        <script type="text/javascript" src="js/sideMenu.js"></script>
    </head>
    <body>
        <div class="jumbotron" id="page-header">
            <h1>ARMS</h1>
            <p>Ataxia Rendering/Monitor Site</p>
            <p>
                This site can be used to visualize 3d position data, as gathered during research on ataxia.<br/>
                Click on the arrow on the right to open the menu.
            </p>
        </div>
        <div class="container-fluid" id="page-content-wrapper">
            <jsp:include page="includes/rendering.jsp"/>
            <jsp:include page="includes/sideMenu.jsp"/>
        </div>


    </body>
</html>
