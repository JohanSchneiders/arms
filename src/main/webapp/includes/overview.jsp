<%--
template for the database overview in the side menu
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- tab bodies -->
<!-- patients tab -->
<div class="tab-pane active container overview-tab" id="patients">
    <jsp:include page="subjectTable.jsp">
        <jsp:param name="subjects" value='patients'/>
    </jsp:include>
</div>
<!-- controls tab -->
<div class="tab-pane container overview-tab" id="controls">
    <jsp:include page="subjectTable.jsp">
        <jsp:param name="subjects" value="controls"/>
    </jsp:include>
</div>
<!-- unknown subjects tab -->
<div class="tab-pane container overview-tab" id="unknown">
    <jsp:include page="subjectTable.jsp">
        <jsp:param name="subjects" value="unknown"/>
    </jsp:include>
</div>

