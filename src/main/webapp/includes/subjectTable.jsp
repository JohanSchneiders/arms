<%--suppress ALL --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
template for a single subject table in the database overview
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<table class="table-active table-striped table-bordered">
    <thead class="thead thead-dark">
        <tr>
            <th></th>
            <th>id</th>
            <th>gender</th>
            <th>age</th>
            <th>specific condition</th>
            <th colspan="2" class="text-center">trajectories</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${requestScope[param.subjects]}" var="subjectEntry">
            <tr>
                <td>
                    <button type="button" class="btn btn-danger delete-subject-button" value="${subjectEntry.key.id}">
                        &times;
                    </button>
                </td>
                <td>${subjectEntry.key.id}</td>
                <td>${subjectEntry.key.gender}</td>
                <td>${subjectEntry.key.age}</td>
                <td>${subjectEntry.key.specificCondition}</td>
                <td>
                    <div class="btn-group-vertical btn-group-sm align-top"><!-- left arm trajectories -->
                        <c:forEach items="${subjectEntry.value.leftArmSpecifiers}" var="specifier">
                            <div class="btn-group">
                                <div class="btn-group" style="width:85%">
                                    <button type="button" class="btn btn-primary trajectory-button" value="${specifier.internalId}">
                                            ${specifier.sensor}
                                    </button>
                                </div>
                                <div class="btn-group" style="width:15%">
                                    <button type="button" class="btn btn-danger delete-trajectory-button" value="${specifier.internalId}">
                                        &times;
                                    </button>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </td>
                <td>
                    <div class="btn-group-vertical btn-group-sm align-top"><!-- right arm trajectories -->
                        <c:forEach items="${subjectEntry.value.rightArmSpecifiers}" var="specifier">
                            <div class="btn-group">
                                <div class="btn-group" style="width:85%">
                                <button type="button" class="btn btn-primary trajectory-button" value="${specifier.internalId}">
                                    ${specifier.sensor}
                                </button>
                                </div>
                                <div class="btn-group" style="width:15%">
                                    <button type="button" class="btn btn-danger delete-trajectory-button" value="${specifier.internalId}">
                                        &times;
                                    </button>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>
