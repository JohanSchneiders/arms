<%--
template for the rendering area
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="rendering">
    <h2 class="clear-both">rendering area</h2>
    <!-- plot card template -->
    <div class="card bg-info text-white plot-card float-left" id="plot-card-template" hidden>
        <div class="card-header">
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="card-body"></div>
        <div class="card-footer"></div>
    </div>
</div>


