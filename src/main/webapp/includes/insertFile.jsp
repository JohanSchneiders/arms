<%--
template for the "new trajectory" tab
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- file upload / data entry section -->
<form id="upload-form" action="/upload" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="patientID">Patient id:</label>
        <input type="number", min="0" step="1" name="patientId" id="patientID" required/>
    </div>
    <div class="form-group">
        <label for="age">Age:</label>
        <input type="number" value="0" min="0" step="1" name="age" id="age"/>
    </div>
    <div class="form-group">
        <label class="radio-inline"><input type="radio" name="gender" value="MALE"/>M</label>
        <label class="radio-inline"><input type="radio" name="gender" value="FEMALE"/>F</label>
        <label class="radio-inline"><input type="radio" name="gender" value="UNKNOWN" checked/>Unknown</label>
    </div>
    <div class="form-group">
        <label class="radio-inline"><input type="radio" name="group" value="PATIENT"/>Patient</label>
        <label class="radio-inline"><input type="radio" name="group" value="CONTROL"/>Control</label>
        <label class="radio-inline"><input type="radio" name="group" value="UNKNOWN" checked/>Unknown</label>
    </div>
    <div class="form-group">
        <label for="con">condition:</label>
        <input type="text" name="condition" id="con"/>
    </div>
    <div class="form-group">
        <label class="radio-inline"><input type="radio" name="arm" value="LEFT" required/>Left</label>
        <label class="radio-inline"><input type="radio" name="arm" value="RIGHT" />Right</label>
    </div>
    <div>
        <input type="file" name="file" multiple required/>
    </div>
    <input type="submit" />
</form>