<%--
template for the side menu.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav id="side-menu" class="hidden-menu container bg-light">
    <button class="button btn-primary no-glow-btn rounded-left" id="side-menu-button">
        <img src="../images/Back_Arrow.svg.png" class="unflipped">
    </button>
    <div id="side-menu-alert-area">
        <!-- all .alert objects are hidden by default(index.css) -->
     <div class="alert alert-dismissible fade show w-50 " id="alert-template" role="alert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
    </div>
    <!-- tab headers -->
    <ul class="nav nav-tabs bg-light" id="menu-tab-headers">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#patients">Patients</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#controls">Controls</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#unknown">Unknown</a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#upload">new trajectory</a>
        </li>
    </ul>
    <!--tab bodies -->
    <div class="tab-content container" id="menu-tab-bodies">
            <jsp:include page="overview.jsp"/><!-- will deliver #patients and #controls -->
        <div class="tab-pane container" id="upload">
           <jsp:include page="insertFile.jsp"/>
        </div>
    </div>
</nav>