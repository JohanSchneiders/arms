/*this script handles:
-toggling/sliding of the side menu and animation of the arrow image
-updating of the overview tables
-uploading new trajectories
-loading full trajectories from server
*/
$(document).ready(function(){
    //constants
    const toggleButton = $("#side-menu-button");
    const arrowImage = toggleButton.find("img");
    const sideMenu = $("#side-menu");
    const uploadForm = $("#upload-form");
    const alertArea = $("#side-menu-alert-area");

    //functions
    function toggleSideMenu(){
        sideMenu.toggleClass("hidden-menu revealed-menu");
        arrowImage.toggleClass("unflipped flipped");
    }

    function fixSideMenuTabs(){
        //fixes buggy behavior of bootstrap tabs after jquery operations on them.
        var tabLinks = $("#menu-tab-headers").find(".nav-link");
        var activeLink = tabLinks.index($(".active"));
        tabLinks.get((activeLink + 1) % tabLinks.length).click();
        tabLinks.get(activeLink).click();
    }

    function updateOverview(){
        //updates the database overview in the side menu
         $.get("/overview", function(responseXml){
             var error = $(responseXml).find("error");
             if(error.length === 0){
                 //replace overview by new one
                 $(".overview-tab").remove();
                 $("#menu-tab-bodies").prepend(responseXml);
             }else{
                alerts.generateAlert(error.html(), "alert-danger", alertArea);
             }
             fixSideMenuTabs();
        });
    }

    function processFileUploadResponse(responseXml){
        //shows appropriate pop-up box after file upload
        console.log(responseXml);
        var error = $(responseXml).find("error");
        if(error.length === 0){
            alerts.generateAlert($(responseXml).find("success").html(),"alert-success", alertArea);
            updateOverview();
        }else{
            alerts.generateAlert(error.html(), "alert-danger", alertArea);
        }
        uploadForm[0].reset();
    }

    function loadTrajectory(){
        //loads trajectory from server and plots it
        var trajectoryId = $(this).val();
        if(render.trajectories.includes(trajectoryId)){
            console.log("trajectory with id " + trajectoryId + "already in render.")
        }else{
            render.trajectories.push(trajectoryId);
            //load trajectory from server
            $.get("/load", {trajectoryId : trajectoryId}, function(response){
                console.log(response);
                if("error" in response){
                    alerts.generateAlert(response.error, "alert-danger", alertArea);
                }else{
                    var specifier = response.specifier; var subject = specifier.subject;
                    var title = [subject.id, subject.gender,subject.experimentalGroup, specifier.arm, specifier.sensor].join(" ");
                    var footer = typeof subject.specificCondition === "undefined" ? "no description" : subject.specificCondition;
                    var plotType = specifier.sensor.split("_").pop();
                    console.log("condition: " + subject.specificCondition);
                    render.plotGraph(title, footer, response.xPositions, response.yPositions, response.zPositions,
                        plotType, specifier.internalId);
                }
            });
        }
    }

    function deleteTrajectory() {
        //deletes selected trajectory from database
        var trajectoryId = $(this).val();
        $.get("/delete", {trajectoryId : trajectoryId}, function (responseXml) {
            var error = $(responseXml).find("error");
            if(error.length ===0){
                alerts.generateAlert($(responseXml).find("success").html(),"alert-success", alertArea);
                updateOverview();
            }else{
                alerts.generateAlert(error.html(), "alert-danger", alertArea)
            }
        });
    }

    function deleteSubject() {
        //deletes selected subject from database, together with all associated trajectories.
        var subjectId = $(this).val();
        $.get("/delete", {subjectId : subjectId}, function (responseXml) {
            var error = $(responseXml).find("error");
            if(error.length ===0){
                alerts.generateAlert($(responseXml).find("success").html(),"alert-success", alertArea);
                updateOverview();
            }else{
                alerts.generateAlert(error.html(), "alert-danger", alertArea)
            }
        })
    }

    //event handlers
    toggleButton.click(toggleSideMenu);
    //events bound in this way, since the buttons are generated through ajax
    sideMenu.on("click", ".trajectory-button", loadTrajectory);
    sideMenu.on("click", ".delete-trajectory-button", deleteTrajectory);
    sideMenu.on("click", ".delete-subject-button", deleteSubject);
    //syntax for jquery.form.js
    uploadForm.ajaxForm(processFileUploadResponse);

    //things to do immediately(main)
    updateOverview();
});