//all rendering/plotting logic goes here
//render is a module/namespace
var render;

$(document).ready(function(){
    //constants
    const PLOTTING_AREA = $("#rendering");
    const PLOT_CARD_TEMPLATE = $("#plot-card-template");

    //functions
    function removePlot(){
        //remove plot id from trajectory array
        var index = render.trajectories.indexOf($(this).val());
        render.trajectories.splice(index, 1);
        //remove plot card from dom
        $(this).closest(".plot-card").remove();
    }

    //event handlers
    PLOTTING_AREA.on("click", ".close", removePlot);

    //module declaration
   render = function() {
        //variables
        var currentPlotId = 1;
        // array to keep track of rendered trajectories. will hold id's
        var trajectories = [];

        //functions

       function plotGraph(header, footer, xCoordinates, yCoordinates, zCoordinates, plotType, plotId) {
           //plots a graph based on what plot type is given.
           //create new plot-card and fill it.
           var newPlotCard = PLOT_CARD_TEMPLATE.clone()
               .attr("id", currentPlotId++)
               .removeAttr("hidden");
           newPlotCard.find(".card-header").prepend(header);
           newPlotCard.find(".card-footer").html(footer);
           newPlotCard.find(".close").val(plotId);

           var cardBody = newPlotCard.find(".card-body")
               .attr("id", "current-card-body");
           PLOTTING_AREA.append(newPlotCard);

           // checks what kind of plot type is given.
           if(plotType === "LOCATION"){
               plotLocation(xCoordinates, yCoordinates, zCoordinates);
           }else {
               plotRotation(xCoordinates, yCoordinates, zCoordinates);
           }

           cardBody.removeAttr("id");
       }

       function getDegrees(radians) {
           //converts radians to degrees
           return((radians/Math.PI)*180)
       }

       function getAnglePlotvalues(angle){
           //returns an array containng [angle, background-angle]
           angle = Math.abs(angle);
           return [angle, 360 - angle]
       }

       function plotRotation(xCoordinates, yCoordinates, zCoordinates) {
           // plots an angle 3D plot for a rotation file
           // Calculates the angles of the first timestamps.
           var firstXAngle = getDegrees(xCoordinates[0]);
           var firstYAngle = getDegrees(yCoordinates[0]);
           var firstZAngle = getDegrees(zCoordinates[0]);
           //plotly has very bulky syntax
           var dataFirstTimestamp = [{
               values: getAnglePlotvalues(firstXAngle),
               name: "Angle on x axis",
               labels: ["X", "."],
               markers: {
                   colors: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)']
                },
                type: "pie",
                domain: {
                    x: [0, .48],
                    y: [0, .49]
                },
                hoverinfo: 'label+value+name',
                textinfo: 'label',
                rotation: firstXAngle < 0 ? 0 : firstXAngle
            }, {
               values: getAnglePlotvalues(firstYAngle),
               name: "Angle on y axis",
               labels: ["Y", "."],
               markers: {
                   colors: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)']
               },
               type: "pie",
               domain: {
                   x: [0.52, 1],
                   y: [0, .49]
               },
               hoverinfo: 'label+value+name',
               textinfo: 'label',
               rotation: firstYAngle < 0 ? 0 : firstYAngle
           }, {
               values: getAnglePlotvalues(firstZAngle),
               name: "Angle on z axis",
               labels: ["Z", "."],
               markers: {
                   colors: ['rgb(0, 0, 0)', 'rgb(255, 255, 255)']
               },
               type: "pie",
               domain: {
                   x: [0, .48],
                   y: [.51, 1]
               },
               hoverinfo: 'label+value+name',
               textinfo: 'label',
               rotation: firstZAngle < 0 ? 0 : firstZAngle
           }];

           //calculate frames for animation
           var frames = [];
           for(var i=0; i < xCoordinates.length; i++){
               var xAngle = getDegrees(xCoordinates[i]);
               var yAngle = getDegrees(yCoordinates[i]);
               var zAngle = getDegrees(zCoordinates[i]);

               frames.push({
                   name:i,
                   data: [{
                       values: getAnglePlotvalues(xAngle),
                       rotation: xAngle < 0 ? 0 : xAngle
                   }, {
                       values: getAnglePlotvalues(yAngle),
                       rotation: yAngle < 0 ? 0 : yAngle
                   }, {
                       values: getAnglePlotvalues(zAngle),
                       rotation: zAngle < 0 ? 0 : zAngle
                   }]
               })
           }
           //couple slider to frames
           var sliderSteps = [];
           for(i=0; i < xCoordinates.length; i++){
               sliderSteps.push({
                   method: 'animate',
                   label: i,
                   args:  [[i], {
                       mode: 'immediate',
                       transition: {duration: 0},
                       frame: {duration: 0, redraw: false}
                   }]
               });
           }
           //define layout of plot
           var layout = {
               width: 400,
               height: 400,
               hovermode: 'closest',
               margin: {
                   l:0,
                   r:0,
                   b:0,
                   t:0
               },
               // the layout of the animation and buttons
               updatemenus: [{
                   x:0,
                   y:0,
                   yanchor:'top',
                   xanchor:'left',
                   showactive: false,
                   direction: 'left',
                   type: 'buttons',
                   pad: {t: 87, r: 10},
                   buttons: [{
                       method: 'animate',
                       args: [null, {
                           mode: 'immediate',
                           fromcurrent: true,
                           transition: {duration: 5},
                           frame: {duration: 100, redraw: false}
                       }],
                       label: 'Play'
                   }, {
                       method: 'animate',
                       args: [[null], {
                           mode: 'immediate',
                           transition: {duration: 0},
                           frame: {duration: 0, redraw: false}
                       }],
                       label: 'Pause'
                   }]
               }],
               // The layout of the sliders
               sliders: [{
                   pad: {l: 130, t: 55},
                   currentvalue: {
                       visible: true,
                       prefix: 'Timestamp:',
                       xanchor: 'right',
                       font: {size: 20, color: '#666'}
                   },
                   transition: {duration: 0},
                   steps: sliderSteps
               }]
           };
           Plotly.plot("current-card-body", {
               data: dataFirstTimestamp,
               layout: layout,
               frames: frames
           });
       }

       function plotLocation(xCoordinates, yCoordinates, zCoordinates) {
           //plots a 3D plot for a location file
           var data = [{
               // The line of the data
               type: 'scatter3d',
               mode: 'lines',
               x: xCoordinates,
               y: yCoordinates,
               z: zCoordinates,
               opacity: 1,
               line: {
                   width: 6,
                   color: "blue",
                   reversescale: false
               }
           }, {
               // The marker of a certain timestamp
               type: 'scatter3d',
               mode: 'markers',
               opacity: 1,
               line: {
                   width: 6,
                   color: "red",
                   reversescale: false
               }
           }];

           // The frames of the animation are created
           var frames = [];
           for(var i=0; i < xCoordinates.length; i++){
               frames.push({
                   name: i,
                   data: [null, {
                       x: [xCoordinates[i]],
                       y: [yCoordinates[i]],
                       z: [zCoordinates[i]]
                   }]
               })
           }

           // The steps of the slider are created
           var sliderSteps = [];
           for(i=0; i < xCoordinates.length; i++){
               sliderSteps.push({
                   method: 'animate',
                   label: i,
                   args:  [[i], {
                       mode: 'immediate',
                       transition: {duration: 0},
                       frame: {duration: 0, redraw: false}
                   }]
               });
           }

           var layout = {
               width: 400,
               height: 400,
               hovermode: 'closest',
               margin: {
                   l:0,
                   r:0,
                   b:0,
                   t:0
               },
               // the layout of the annimation and buttons
               updatemenus: [{
                   x:0,
                   y:0,
                   yanchor:'top',
                   xanchor:'left',
                   showactive: false,
                   direction: 'left',
                   type: 'buttons',
                   pad: {t: 87, r: 10},
                   buttons: [{
                       method: 'animate',
                       args: [null, {
                           mode: 'immediate',
                           fromcurrent: true,
                           transition: {duration: 5},
                           frame: {duration: 100, redraw: false}
                       }],
                       label: 'Play'
                   }, {
                       method: 'animate',
                       args: [[null], {
                           mode: 'immediate',
                           transition: {duration: 0},
                           frame: {duration: 0, redraw: false}
                       }],
                       label: 'Pause'
                   }]
               }],
               // The layout of the sliders
               sliders: [{
                   pad: {l: 130, t: 55},
                   currentvalue: {
                       visible: true,
                       prefix: 'Timestamp:',
                       xanchor: 'right',
                       font: {size: 20, color: '#666'}
                   },
                   transition: {duration: 0},
                   steps: sliderSteps
               }]
           };
           Plotly.plot("current-card-body", {
               data: data,
               layout: layout,
               frames: frames
           });
       }


        //only variables/objects in the return object are 'public'
        return {
            trajectories : trajectories,
            plotGraph: plotGraph
        }
    }();
});