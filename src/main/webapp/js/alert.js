/*this script provides a function for generating bootstrap 4 alerts: alerts.generateAlert(message, contextClass, parent);
* this script assumes bootstrap 4.0.0-beta.2 css and js, popper 1.12.6 js and jquery 3.2.1 js
* *this script requires a div #alert-template with class .alert*/
//alerts is a module/namespace
var alerts;

$(document).ready(function(){
    alerts = function(){
        //constants
        const ALERT_TEMPLATE = $("#alert-template");
        ALERT_TEMPLATE.removeAttr("id");

        //public state
        return{
            generateAlert: function(message, contextClass, parent){
                /*generates a dismissible bootstrap 4 alert.
                * args:
                * -message: message/content to put in the alert.
                * -contextClass: "alert-primary" ect. (see bootstrap doc.).
                * -parent (jQuery) object to append the alert to.
                * !! assumes all .alert objects have display:none;
                * */
                newAlert = ALERT_TEMPLATE.clone().addClass(contextClass);
                newAlert.prepend(message);
                parent.append(newAlert);
                newAlert.show("fast");
            }};
    }();
});

