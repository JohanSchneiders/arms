<%--
  Created by IntelliJ IDEA.
  User: ddubber
  Date: 22-12-17
  Time: 9:00
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="application/xml" pageEncoding="UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- template for ajax response -->
<points>
    <c:forEach var="point" items="${requestScope.points}">
        <point>
            <x>${point.xpos}</x>
            <y>${point.ypos}</y>
            <z>${point.zpos}</z>
        </point>
    </c:forEach>
</points>
