package model;

/**
 * Class representing a single point in space and time.
 */
public class Point implements Comparable<Point> {
    private int timestamp;
    private double xPos, yPos, zPos;

    /**
     * constructor.
     *
     * @param timestamp time at which the point was measured.
     * @param x         x-coordinate of the point.
     * @param y         y-coordinate of the point.
     * @param z         x-coordinate of the point.
     */
    public Point(int timestamp, double x, double y, double z) {
        this.timestamp = timestamp;
        this.xPos = x;
        this.yPos = y;
        this.zPos = z;
    }

    /**
     * Compares this to another point.
     * The point with the largest timestamp is the biggest one.
     *
     * @param o point to compare to/
     * @return -1 if this.timestamp < o.timestamp,
     * 0 if this.timestamp == o.timestamp,
     * 1 if this.timestamp > o.timestamp</>
     */
    @Override
    public int compareTo(Point o) {
        return Integer.compare(this.timestamp, o.timestamp);
    }

    /**
     * @return String representation of the Point.
     */
    @Override
    public String toString() {
        return "Point{" +
                "timestamp=" + timestamp +
                ", xPos=" + xPos +
                ", yPos=" + yPos +
                ", zPos=" + zPos +
                '}';
    }

    /**
     * @return the timestamp of the point.
     */
    public int getTimestamp() {
        return timestamp;
    }

    /**
     * @return The x-coordinate of the point.
     */
    public double getXpos() {
        return xPos;
    }

    /**
     * @return The y-coordinate of the point.
     */
    public double getYpos() {
        return yPos;
    }

    /**
     * @return the z-coordinate of the point.
     */
    public double getZpos() {
        return zPos;
    }
}
