package model;

public enum Sensor {
    FOREARM_LOCATION,
    ELBOW_ROTATION,
    UPPER_ARM_LOCATION,
    WRIST_ROTATION,
    HAND_LOCATION;
}
