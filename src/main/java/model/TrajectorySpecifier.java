package model;


import com.sun.istack.internal.NotNull;

/**
 * Class describing the experimental origin of a recorded trajectory.
 */
public class TrajectorySpecifier {
    private Subject subject;
    private Arm arm;
    private Sensor sensor;
    private int internalId;

    /**
     * constructor
     * @param subject subject from which the trajectory is
     * @param arm arm from which the trajectory is
     * @param sensor 'sensor' from which the trajectory is
     */
    public TrajectorySpecifier(@NotNull Subject subject, @NotNull Arm arm, @NotNull Sensor sensor){
        this(subject, arm, sensor, 0);
    }

    /**
     * constructor
     * @param subject subject from which the trajectory is
     * @param arm arm from which the trajectory is
     * @param sensor 'sensor' from which the trajectory is
     *@param internalId internal database id of the trajectory. defaults to 0. Do not set this unless the id is read from database.
     */
    public TrajectorySpecifier(@NotNull Subject subject, @NotNull Arm arm, @NotNull Sensor sensor, int internalId){
        if(subject == null || arm == null || sensor == null){
            throw new IllegalArgumentException("Received null as an argument");
        }
        this.subject = subject;
        this.arm = arm;
        this.sensor = sensor;
        this.internalId = internalId;
    }

    //getters

    /**
     * @return the subject to which this trajectory belongs.
     */
    public Subject getSubject() {
        return subject;
    }

    /**
     * @return the arm of the subject on which the trajectory was measured.
     */
    public Arm getArm() {
        return arm;
    }

    /**
     * @return the sensor on the arm of the subject whith which this trajectory was recorded.
     */
    public Sensor getSensor() {
        return sensor;
    }

    /**
     * @return A String representation of the trajectory.
     */

    /**
     * @return the internal database id of the trajectory. defaults to 0
     */
    public int getInternalId() {
        return internalId;
    }

    @Override
    public String toString() {
        return "TrajectorySpecifier{" +
                "subject=" + subject +
                ", arm=" + arm +
                ", sensor=" + sensor +
                ", internalId=" + internalId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrajectorySpecifier specifier = (TrajectorySpecifier) o;

        if (!subject.equals(specifier.subject)) return false;
        if (arm != specifier.arm) return false;
        return sensor == specifier.sensor;
    }

    @Override
    public int hashCode() {
        int result = subject.hashCode();
        result = 31 * result + arm.hashCode();
        result = 31 * result + sensor.hashCode();
        return result;
    }

}
