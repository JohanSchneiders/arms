package model;

/**
 * Enum signalling in which experimental group a given subject was.
 */
public enum ExperimentalGroup {
    CONTROL,
    PATIENT,
    UNKNOWN;
}
