package model;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a path through space.
 */
public class Trajectory {
    private TrajectorySpecifier specifier;
    private ArrayList<Double> xPositions = new ArrayList<>(),
            yPositions = new ArrayList<>(),
            zPositions = new ArrayList<>();

    /**
     * constructor
     * @param specifier TrajectorySpecifier describing the experimental origin of the trajectory.
     */
    public Trajectory(@NotNull TrajectorySpecifier specifier){
        if(specifier == null){
            throw new IllegalArgumentException("Received null for TrajectorySpecifier");
        }
        this.specifier = specifier;
    }

    /**
     * Adds a Point to the end of the trajectory.
     */
    public void addPoint(Point point){
        addPoint(point.getXpos(), point.getYpos(), point.getZpos());
    }

    /**
     * adds a point to the end of the trajectory.
     * @param xPos x position of the point to add.
     * @param yPos y position of the point to add.
     * @param zPos z position of the point to add.
     */
    public void addPoint(double xPos, double yPos, double zPos){
        this.xPositions.add(xPos);
        this.yPositions.add(yPos);
        this.zPositions.add(zPos);
    }

    /**
     * @return list containing Points, in the order they were inserted.
     * the timestamp of each point will be the index of the point within the trajectory.
     */
    public List<Point> getPoints(){
        ArrayList<Point> points = new ArrayList<>();
        for(int i=0; i < this.xPositions.size(); i++){
            points.add(new Point(i, xPositions.get(i), yPositions.get(i), zPositions.get(i)));
        }
        return points;
    }

    /**
     * @return TrajectorySpecifier describing the experimental origin of the trajectory.
     */
    public TrajectorySpecifier getSpecifier() {
        return specifier;
    }

    @Override
    public String toString() {
        return "Trajectory{" +
                "specifier=" + specifier +
                ", number of points:" + zPositions.size() +
                '}';
    }
}
