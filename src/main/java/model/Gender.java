package model;

public enum Gender {
    MALE("M"),
    FEMALE("F"),
    UNKNOWN("U");

    private String abbreviation;

    private Gender(String abbreviation){
        this.abbreviation = abbreviation;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}
