package model;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

/**
 * class representing a test subject(person).
 */
public class Subject {
    private int id;
    private int age;
    private Gender gender;
    private ExperimentalGroup experimentalGroup;
    private String specificCondition;

    /**
     * constructor
     *
     * @param id                id of subject
     * @param gender            gender of subject
     * @param experimentalGroup Experimental Group that the subject was in
     * @param specificCondition short description of the specific condition the subject has
     */
    public Subject(int id, @Nullable Integer age, @Nullable Gender gender, @Nullable ExperimentalGroup experimentalGroup, @Nullable String specificCondition) {
        this.id = id;
        this.age = age;
        this.gender = gender;
        this.experimentalGroup = experimentalGroup;
        this.specificCondition = specificCondition;
    }

    /**
     * @return the id of the subject.
     */
    public int getId() {
        return id;
    }

    /**
     * @return the gender of the subject.
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * @return the experimental group the subject was in.
     */
    public ExperimentalGroup getExperimentalGroup() {
        return experimentalGroup;
    }

    /**
     * @return a more specific description of the subjects condition
     */
    public String getSpecificCondition() {
        return specificCondition;
    }

    /**
     * @return the age of the subject at time of recording.
     */
    public int getAge() {
        return age;
    }

    /**
     * @return String representation of the Subject.
     */
    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", gender=" + gender +
                ", experimentalGroup=" + experimentalGroup +
                ", specificCondition='" + specificCondition + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject subject = (Subject) o;

        return id == subject.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
