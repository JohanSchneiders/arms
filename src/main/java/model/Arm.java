package model;

/**
 * Enum with values that represent the left or the right arm.
 */
public enum Arm {
    LEFT("L"),
    RIGHT("R");

    private String abbreviation;

    /**
     * constructor
     * @param abbreviation abbreviation for the arm(used in database)
     */
    Arm(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    /**
     *
     * @return the abbreviation for the arm.
     */
    public String getAbbreviation() {
        return abbreviation;
    }
}
