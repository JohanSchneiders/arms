package servlets;

import com.google.gson.Gson;
import exceptions.DatabaseException;
import model.Trajectory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * servlet that loads a trajectory from the database based on id and serves it as a JSON object.
 */
@WebServlet(name = "ChartServlet", urlPatterns = "/load")
public class ChartServlet extends DatabaseServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonString = "";
        try {
            //load trajectory from database
            int trajectoryId = Integer.parseInt(request.getParameter("trajectoryId"));
            Trajectory trajectory = database.getTrajectory(trajectoryId);
            jsonString = new Gson().toJson(trajectory);
        } catch (DatabaseException e) {
            jsonString = String.format("{\"error\": \"%s\"}", e.getMessage());
        }finally{
            //write JSON response text
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(jsonString);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //post is not supported
    }
}
