package servlets;

import Parsers.SensorDataParser;
import exceptions.DatabaseException;
import exceptions.ParseException;
import model.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Collection;

@WebServlet(name = "UploadServlet", urlPatterns = "/upload")
@MultipartConfig
/**
 * servlet that handles uploading(and saving to database) of new ataxia data files.
 */
public class UploadServlet extends DatabaseServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dispatchPage = "/xml/successfulUpload.jsp";
        try{
            request.setAttribute("numberOfFiles", uploadTrajectoryFiles(request));
        } catch (ParseException | DatabaseException e) {
            request.setAttribute("error", e);
            dispatchPage = "/xml/error.jsp";
        }finally{
            RequestDispatcher view = request.getRequestDispatcher(dispatchPage);
            view.forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("arrived in");
    }

    /**
     * parses and saves trajectory files.
     * @param request request comming from the client
     * @return number of files saved.
     * @throws ServletException if the request could not be processed.
     * @throws DatabaseException if the parsed files could not be saved to the database.
     * @throws ParseException if given files(in request) could not be parsed.
     * @throws IOException if given files(in request) could not be opened/read.
     */
    private int uploadTrajectoryFiles(HttpServletRequest request)
            throws ServletException, DatabaseException, ParseException, IOException {
        Collection<Part> fileParts = request.getParts();
        fileParts.removeIf(p -> !p.getName().equals("file"));
        //loop through files
        for (Part part : fileParts){
            //setup Subject ToDo check for null input value?
            Subject subject = new Subject(
                    Integer.parseInt(request.getParameter("patientId")),
                    Integer.parseInt(request.getParameter("age")),
                    Gender.valueOf(request.getParameter("gender")),
                    ExperimentalGroup.valueOf(request.getParameter("group")),
                    request.getParameter("condition"));
            //parse file
            SensorDataParser parser = new SensorDataParser(part.getSubmittedFileName(), part.getInputStream());
            //setup TrajectorySpecifier and Trajectory
            Trajectory trajectory = new Trajectory(
                    new TrajectorySpecifier(subject, Arm.valueOf(request.getParameter("arm")), parser.getSensor()));
            parser.getPoints().forEach(trajectory::addPoint);
            //save trajectory
            database.saveTrajectory(trajectory);
        }
        return fileParts.size();
    }

}
