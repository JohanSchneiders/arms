package servlets;
import DAO.DummyDatabase;
import DAO.MySQLTrajectoryLoader;
import DAO.TrajectoryDataSource;
import exceptions.DatabaseException;
import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * abstract superclass for all servlets that should connect to a database.
 * the init() method
 */
public abstract class DatabaseServlet extends HttpServlet {
    protected TrajectoryDataSource database;

    protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
    protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            switch(getServletContext().getInitParameter("db_type")) {
                case "MySQL":
                    //  read database credentials
                    DbUser user = DbCredentials.getMySQLuser();
                    //connect to database
                    database = MySQLTrajectoryLoader.getLoader(
                            user.getHost(),
                            user.getDatabaseName(),
                            user.getUserName(),
                            user.getDatabasePassword());
                    break;
                case "dummy":
                    database = DummyDatabase.getLoader();
                    break;
                default:
                    System.out.println("no suitable database type selected");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        //disconnect from database
        try {
            database.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        super.destroy();
    }
}
