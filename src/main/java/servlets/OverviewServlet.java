package servlets;

import DAO.SpecifierGroup;
import exceptions.DatabaseException;
import model.*;
import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static model.ExperimentalGroup.CONTROL;
import static model.ExperimentalGroup.PATIENT;
import static model.ExperimentalGroup.UNKNOWN;

@WebServlet(name = "OverviewServlet", urlPatterns = "/overview")
@MultipartConfig
/**
 * servlet that serves the html for the database overview.
 */
public class OverviewServlet extends DatabaseServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dispatchPage = "/includes/overview.jsp";
        Map<Subject, SpecifierGroup> specifiers;
        Map<ExperimentalGroup, HashMap<Subject, SpecifierGroup>> groups = new HashMap<>();
        groups.put(CONTROL, new HashMap<>());
        groups.put(PATIENT, new HashMap<>());
        groups.put(UNKNOWN, new HashMap<>());
        try {
            //get available trajectories from database
            specifiers = database.getAvailableTrajectorySpecifiers();
            //sort/group subjects
            specifiers.forEach((k, v) -> groups.get(k.getExperimentalGroup()).put(k, v));
            //set request attributes
            request.setAttribute("patients",  groups.get(PATIENT));
            request.setAttribute("controls", groups.get(CONTROL));
            request.setAttribute("unknown", groups.get(UNKNOWN));
        } catch (DatabaseException e) {
            request.setAttribute("error", e);
            dispatchPage = "/xml/error.jsp";
        }finally {
            //forward view
            RequestDispatcher view = request.getRequestDispatcher(dispatchPage);
            view.forward(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("arrived in OverviewServlet.post");
        //ToDo remove ugly delegation
        doGet(request, response);
    }

}
