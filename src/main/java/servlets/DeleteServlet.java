package servlets;

import exceptions.DatabaseException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * servlet that deletes either all trajectories linked to a given subject, or a single trajectory from the database.
 */
@WebServlet(name = "DeleteServlet", urlPatterns = "/delete")
public class DeleteServlet extends DatabaseServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String dispatchPage = "/xml/succesfulDelete.jsp";
      try {
          if (request.getParameter("trajectoryId") != null) {
              request.setAttribute("deletes", deleteTrajectory(request));
          } else {
              if (request.getParameter("subjectId") != null) {
                  request.setAttribute("deletes", deleteSubject(request));
              }
          }
      }catch (DatabaseException e){
          request.setAttribute("error", e);
          dispatchPage = "/xml/error.jsp";
      }finally{
          RequestDispatcher view = request.getRequestDispatcher(dispatchPage);
          view.forward(request, response);
      }
    }

    private String deleteSubject(HttpServletRequest request) throws DatabaseException {
        String subjectIdString = request.getParameter("subjectId");
        int subjectId = Integer.parseInt(subjectIdString);
        database.deleteSubject(subjectId);
        return ("Subject with id "+subjectIdString);
    }

    private String deleteTrajectory(HttpServletRequest request) throws DatabaseException {
        String trajectoryIdString = request.getParameter("trajectoryId");
        int trajectoryId = Integer.parseInt(trajectoryIdString);
        database.deleteTrajectory(trajectoryId);
        return ("Trajectory with id "+trajectoryIdString);
    }
}
