package exceptions;

/**
 * Exception that signals problems while accessing a database.
 * exception is checked.
 */
public class DatabaseException extends Exception{
    //constructors
    public DatabaseException() {
    }
    public DatabaseException(String s) {
        super(s);
    }

    public DatabaseException(Throwable throwable) {
        super(throwable);
    }

    public DatabaseException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DatabaseException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
