package DAO;

import model.TrajectorySpecifier;

import java.util.ArrayList;

import static model.Arm.LEFT;

/**
 * wrapper class for holding and sorting TrajectorySpecifiers related to a single subject
 */
public class SpecifierGroup {
    private ArrayList<TrajectorySpecifier> leftArmSpecifiers = new ArrayList<>();
    private ArrayList<TrajectorySpecifier> rightArmSpecifiers = new ArrayList<>();

    /**
     * adds given TrajectorySpecifier to the collection
     * @param specifier TrajectorySpecifier to add.
     */
    public void add(TrajectorySpecifier specifier){
        (specifier.getArm().equals(LEFT)? leftArmSpecifiers : rightArmSpecifiers).add(specifier);
    }

    /**
     * @return all left arm TrajectorySpecifiers in the collection
     */
    public ArrayList<TrajectorySpecifier> getLeftArmSpecifiers() {
        return leftArmSpecifiers;
    }

    /**
     * @return all right arm TrajectorySpecifiers in the collection
     */
    public ArrayList<TrajectorySpecifier> getRightArmSpecifiers() {
        return rightArmSpecifiers;
    }

    @Override
    public String toString() {
        return "SpecifierGroup{" +
                "leftArmSpecifiers=" + leftArmSpecifiers +
                ", rightArmSpecifiers=" + rightArmSpecifiers +
                '}';
    }
}

