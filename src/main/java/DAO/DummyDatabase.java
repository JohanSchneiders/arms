package DAO;

import exceptions.DatabaseException;
import model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static model.Arm.LEFT;
import static model.Arm.RIGHT;
import static model.ExperimentalGroup.CONTROL;
import static model.ExperimentalGroup.PATIENT;
import static model.Gender.FEMALE;
import static model.Gender.MALE;
import static model.Sensor.*;

/**
 * this class implements the TrajectoryDatasource, without requiring a connection to an actual (MySQL) database.
 * All data saved to it will be lost at un-deployment of the web app.
 */
public class DummyDatabase implements TrajectoryDataSource {
    private ArrayList<Trajectory> trajectories = new ArrayList<>();
    private static DummyDatabase singleInstance = null;
    private int autoIncrementValue = 1;

    /**
     * singleton pattern
     * @return single instance of DummyDatabase
     * @throws DatabaseException if the instance could not be initialized.
     */
    public static DummyDatabase getLoader() throws DatabaseException {
        if(singleInstance == null){
            singleInstance = new DummyDatabase();
            return singleInstance;
        }
        return singleInstance;
    }


    /**
     * constructor
     * @throws DatabaseException if the database could not be instantiated.
     */
    private DummyDatabase() throws DatabaseException {
        connect();
    }

    /**
     * "connects" to the database. sets up dummy data.
     * @throws DatabaseException part of interface. Should never happen.
     */
    @Override
    public void connect() throws DatabaseException {
        //create several new subjects and trajectorySpecifiers:
        //fill trajectories with points
        //contols
        Subject s1 = new Subject(1, 21, MALE, CONTROL, null);
        saveTrajectory(new Trajectory(new TrajectorySpecifier(s1, LEFT, WRIST_ROTATION)));
        saveTrajectory(new Trajectory(new TrajectorySpecifier(s1, LEFT, HAND_LOCATION)));
        saveTrajectory(new Trajectory(new TrajectorySpecifier(s1, RIGHT, ELBOW_ROTATION)));

        Subject s2 = new Subject(2, 30, FEMALE, CONTROL, null);
        saveTrajectory(new Trajectory(new TrajectorySpecifier(s2, LEFT, WRIST_ROTATION)));

        //patients
        Subject s3 = new Subject(3, 19, FEMALE, PATIENT, "slight tremors");
        saveTrajectory(new Trajectory(new TrajectorySpecifier(s3, LEFT, WRIST_ROTATION)));

        Subject s4 = new Subject(4, 21, MALE, PATIENT, null);
        saveTrajectory(new Trajectory(new TrajectorySpecifier(s4, LEFT, WRIST_ROTATION)));
        saveTrajectory(new Trajectory(new TrajectorySpecifier(s4, LEFT, HAND_LOCATION)));
        saveTrajectory(new Trajectory(new TrajectorySpecifier(s4, RIGHT, ELBOW_ROTATION)));
        saveTrajectory(new Trajectory(new TrajectorySpecifier(s4, RIGHT, FOREARM_LOCATION)));

        //add points to trajectories
        trajectories.forEach(this::addSomePointsToTrajectory);
        System.out.println("connected to database");
    }


    /**
     * generates sample data and adds it to given trajectory.
     * @param trajectory trajectory to add points to.
     */
    private void addSomePointsToTrajectory(Trajectory trajectory) {
        int n = 500;

        for(double time = 0.0; time < n; time++){
            trajectory.addPoint(
                    (time / n - 0.5) * 4,
                    2.0 * Math.cos(time / 100),
                    2.0 * Math.sin(time / 50));
        }
    }

    /**
     * disconnects from the database. part of interface, doesn't actually do anything.
     * @throws DatabaseException never.
     */
    @Override
    public void disconnect() throws DatabaseException {
        System.out.println("disconnected from database.");
    }

    /**
     * loads a complete trajectory given a TrajectorySpecifier.
     * @param specifier TrajectorySpecifier
     * @return filled Trajectory
     * @throws DatabaseException if specified Trajectory could not be found.
     */
    @Override
    public Trajectory getTrajectory(TrajectorySpecifier specifier) throws DatabaseException {
        for(Trajectory trajectory : trajectories){
            if(trajectory.getSpecifier().equals(specifier)){
                return trajectory;
            }
        }
        throw new DatabaseException("no such trajectory");
    }

    /**
     * loads a complete trajectory given an internal id
     * @param id internal id of the trajectory to be removed.
     * @return filled Trajectory
     * @throws DatabaseException if no trajectory with given internal id was found
     */
    @Override
    public Trajectory getTrajectory(int id) throws DatabaseException {
        for(Trajectory trajectory : trajectories){
            if(trajectory.getSpecifier().getInternalId() == id){
                return trajectory;
            }
        }
        throw new DatabaseException("found no trajectory with id " + id);
    }

    /**
     * @return map linking a subject to all trajectories of that subject
     * @throws DatabaseException never
     */
    @Override
    public Map<Subject, SpecifierGroup> getAvailableTrajectorySpecifiers() throws DatabaseException {
        Map<Subject, SpecifierGroup> specifiers = new HashMap<>();
        TrajectorySpecifier specifier;
        Subject subject;
        for(Trajectory trajectory : trajectories){
            specifier = trajectory.getSpecifier();
            subject = specifier.getSubject();
            if(!specifiers.containsKey(subject)){
                specifiers.put(subject, new SpecifierGroup());
            }
            specifiers.get(subject).add(specifier);
        }
        return specifiers;
    }

    /**\
     * saves given trajectory to the database
     * @param trajectory trajectory to be saved
     * @throws DatabaseException if saving leads to a duplicate entry(currently not implemented for DummyDatabase)
     */
    @Override
    public void saveTrajectory(Trajectory trajectory) throws DatabaseException {
        //create copy of trajectory, with internal id added
        TrajectorySpecifier oldSpecifier = trajectory.getSpecifier();
        TrajectorySpecifier newSpecifier = new TrajectorySpecifier(
                oldSpecifier.getSubject(),
                oldSpecifier.getArm(),
                oldSpecifier.getSensor(),
                autoIncrementValue++
                );
        Trajectory newTrajectory= new Trajectory(newSpecifier);
        trajectory.getPoints().forEach(newTrajectory::addPoint);
        trajectories.add(newTrajectory);
    }

    /**
     * removes trajectory with given internal id
     * @param trajectoryId id of trajectory to be removed.
     * @throws DatabaseException never
     */
    @Override
    public void deleteTrajectory(int trajectoryId) throws DatabaseException {
        trajectories.removeIf(trajectory -> trajectory.getSpecifier().getInternalId() == trajectoryId);
    }

    /**
     * removes all trajectories that are linked to given subject from the database.
     * @param subjectId id of the subject to be removed.
     * @throws DatabaseException never.
     */
    @Override
    public void deleteSubject(int subjectId) throws DatabaseException {
        trajectories.removeIf(trajectory -> trajectory.getSpecifier().getSubject().getId() == subjectId);
    }

}
