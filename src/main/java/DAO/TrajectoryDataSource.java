package DAO;

import exceptions.DatabaseException;
import model.Subject;
import model.Trajectory;
import model.TrajectorySpecifier;

import java.util.Map;

/**
 * Interface to any objects that can save and load Trajectories.
 */
public interface TrajectoryDataSource {
    void connect() throws DatabaseException;
    void disconnect() throws DatabaseException;
    Trajectory getTrajectory(TrajectorySpecifier specifier) throws DatabaseException;
    Trajectory getTrajectory(int id) throws DatabaseException;
    Map<Subject, SpecifierGroup> getAvailableTrajectorySpecifiers() throws DatabaseException;
    void saveTrajectory(Trajectory trajectory) throws DatabaseException;
    void deleteTrajectory(int trajectoryId) throws DatabaseException;
    void deleteSubject(int subjectId) throws DatabaseException;
}
