package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Set;

/**
 * class that can create, store and execute prepared statements on a MySQL server.
 */
public class PreparedStatementManager {
    private Connection connection;
    private HashMap<String, PreparedStatement> statements = new HashMap<>();

    /**
     * constructor.
     * @param connection active connection to a MySQL database.
     */
    public PreparedStatementManager(Connection connection) {
        this.connection = connection;
        }

    /**
     * //(re)place prepared statement in the manager
     * @param name name of the prepared statement
     * @param query sql (body) of the prepared statement
     * @throws SQLException if the statement could not be created(connection failed ect.)
     */
    void createStatement(String name, String query) throws SQLException {

        if(statements.containsKey(name)){
            statements.get(name).close();
        }
        statements.put(name, connection.prepareStatement(query));
    }

    public ResultSet executeStatement(String name, Object... arguments) throws SQLException {
        PreparedStatement statement = statements.get(name);
        if(statement == null) {
            throw new IllegalArgumentException("PreparedStatementManager does not"
                    + " contain a statement called " + name);
        }
        //set parameters of prepared statement
        for(int i = 0;
            i < statement.getParameterMetaData().getParameterCount();
            i++){
            statement.setObject(i+1, arguments[i]);
        }
        //execute statement
        statement.execute();
        return statement.getResultSet();
    }

    /**
     * closes and removes prepared statement from the manager
     * @param name name of prepared statement to remove.
     * @throws SQLException if given statement could not be removed(no connection ect.)
     */
    public void removeStatement(String name) throws SQLException {
        if(statements.containsKey(name)){
            statements.get(name).close();
            statements.remove(name);
        }else{
           throw new IllegalArgumentException("PreparedStatementManager does not"
                    + " contain a statement called " + name);
        }
    }

    /**
     * closes all PreparedStatements contained within self
     * @throws SQLException if the statements could not be closed.
     */
    public void close() throws SQLException {
        for(String name : statements.keySet()){
            statements.get(name).close();
        }
    }

    /**
     * @return a set containing all statement-names
     */
    public Set<String> getStatementNames(){
        return statements.keySet();
    }
}
