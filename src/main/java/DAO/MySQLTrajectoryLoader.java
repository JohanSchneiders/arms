package DAO;

import exceptions.DatabaseException;
import model.*;
import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;

import java.io.IOException;
import java.sql.*;
import java.util.*;

/**
 * Class to interact with a MySQL server specifically designed for the ARMs project.
 * implemented by Johan Schneiders.
 * all methods assume the database schema defined in /mySQL/database_design.sql.
 */
public class MySQLTrajectoryLoader implements TrajectoryDataSource {
    private static MySQLTrajectoryLoader instance;

    private String dbUrl = "";
    private String dbUser = "";
    private String dbPassword = "";
    private Connection connection;
    private PreparedStatementManager statements;

    /**
     * Gets an instance of the class MySqlTrajectoryLoader.
     * makes sure there is only one instance present at any given time.
     * @param hostName hostname of database server.
     * @param dbName   name of database schema to use.
     * @param user     username to log in woth.
     * @param password password to log in woth
     * @return MySQLTrajectoryLoader as an interface to the database.
     * @throws DatabaseException if connecting to the database failed.
     */
    public static MySQLTrajectoryLoader getLoader(String hostName, String dbName, String user, String password) throws DatabaseException {
        if (instance == null) {
            String url = String.format("jdbc:mysql://%s/%s", hostName, dbName);
            instance = new MySQLTrajectoryLoader(url, user, password);
        }
        return instance;
    }

    /**
     * private constructor
     *
     * @param url      url to the specific database
     * @param user     username for connecting to database server
     * @param password password for connecting to database server
     * @throws DatabaseException if the database could not be accessed
     */
    private MySQLTrajectoryLoader(String url, String user, String password) throws DatabaseException {
        this.dbUrl = url;
        this.dbUser = user;
        this.dbPassword = password;

        connect();
        this.statements = new PreparedStatementManager(connection);
        setupStatements();
    }

    /**
     * connects this to the database server.
     *
     * @throws DatabaseException if the conection could not be made.
     */
    @Override
    public void connect() throws DatabaseException {
        try {
            if (connection == null || connection.isClosed()) {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
                System.out.println("database connection established");
            }
        } catch (ClassNotFoundException | SQLException e) {
            throw new DatabaseException("Could not connect to database: " + e.getMessage(), e);
        }
    }

    /**
     * Closes the connection with the database.
     *
     * @throws DatabaseException If the connection could not be closed.
     * (usually if the connection was already closed.)
     */
    @Override
    public void disconnect() throws DatabaseException {
        try {
            statements.close();
            connection.close();
            System.out.println("database connection closed");
        } catch (SQLException e) {
            throw new DatabaseException("Could not disconnect from server: " + e.getMessage(), e);
        }
    }

    /**
     * sets up preparedStatements in this.statements
     */
    private void setupStatements() throws DatabaseException {
        try {
            //getters
            statements.createStatement("test", "SELECT date();");
            statements.createStatement("get_subject_ids", "SELECT id FROM subjects");
            statements.createStatement("get_last_inserted_id", "SELECT LAST_INSERT_ID();");
            statements.createStatement("getAvailableTrajectorySpecifiers", "SELECT subject, age, gender, experimental_group, specific_condition, arm, sensor, t.id FROM subjects s JOIN trajectories t ON t.subject = s.id ORDER BY subject, arm, sensor;");
            statements.createStatement("get_trajectory_id", "SELECT id FROM trajectories WHERE subject=? AND arm=? AND  sensor=?;");
            statements.createStatement("get_trajectory_points", "SELECT time, xpos, ypos, zpos FROM points WHERE trajectory=?;");
            statements.createStatement("get_specifierDetails", "SELECT  t.id AS trajectoryId, s.id AS subjectId, arm, sensor , age, gender, experimental_group, specific_condition  FROM trajectories t JOIN subjects s ON t.subject = s.id HAVING t.id = ?");
            //setters
            statements.createStatement("add_subject", "INSERT INTO subjects(id, age, gender, experimental_group, specific_condition) VALUES(?, ?, ?, ?, ?);");
            statements.createStatement("add_trajectory", "INSERT INTO trajectories(subject, arm, sensor) VALUES(?, ?, ?);");
            statements.createStatement("add_point", "INSERT INTO points(trajectory, time, xpos, ypos, zpos) VALUES(?, ?, ?, ?, ?);");
            // deletes
            statements.createStatement("delete_points", "DELETE p FROM points p WHERE p.trajectory=?;");
            statements.createStatement("delete_points_of_subject", "DELETE p FROM points p JOIN trajectories t ON p.trajectory = t.id WHERE t.subject=?;");
            statements.createStatement("delete_trajectory", "DELETE t FROM trajectories t WHERE t.id=?;");
            statements.createStatement("delete_trajectories", "DELETE t FROM trajectories t WHERE t.subject=?");
            statements.createStatement("delete_subject", "DELETE s FROM subjects s WHERE s.id=?;");
        } catch (SQLException e) {
            throw new DatabaseException("Could not setup prepared statements: " + e.getMessage(), e);
        }
    }

    /**
     * Loads a trajectory(including points if there are any) based on given TrajectorySpecifier.
     * @param specifier TrajectorySpecifier describing what Trajectory to get.
     * @return trajectory(including points if there are any) based on given TrajectorySpecifier.
     * @throws DatabaseException If the trajectory could not be retrieved or does not exist in the database.
     */
    @Override
    public Trajectory getTrajectory(TrajectorySpecifier specifier) throws DatabaseException {
        Trajectory trajectory = new Trajectory(specifier);
        try {
            ResultSet points = statements.executeStatement("get_trajectory_points", getTrajectoryId(specifier));
            while(points.next()){
                trajectory.addPoint(
                    points.getDouble("xpos"),
                    points.getDouble("ypos"),
                    points.getDouble("zpos"));
            }
            return trajectory;
        } catch (SQLException e) {
            throw new DatabaseException("Could not get points for trajectory: " + e.getMessage(), e);
        }
    }

    @Override
    public Trajectory getTrajectory(int id) throws DatabaseException {
        try {
            //SELECT  t.id AS trajectoryId, s.id AS subjectId, arm, sensor , age, gender, experimental_group, specific_condition
            ResultSet results = statements.executeStatement("get_specifierDetails", id);
            if(results.next()){
                //create new TrajectorySpecifier
                TrajectorySpecifier specifier = new TrajectorySpecifier(
                        new Subject(
                                results.getInt("subjectId"),
                                results.getInt("age"),
                                Gender.valueOf(results.getString("gender")),
                                ExperimentalGroup.valueOf(results.getString("experimental_group")),
                                results.getString("specific_condition")),
                        Arm.valueOf(results.getString("arm")),
                        Sensor.valueOf(results.getString("sensor")),
                        results.getInt("trajectoryId")
                );
                return getTrajectory(specifier);
            }else{
                throw new DatabaseException("Found no Trajectory with id " + id);
            }
        } catch (SQLException e) {
            throw new DatabaseException("Could not get Trajectory: " + e.getMessage(), e);
        }
    }

    /**
     * gets he internal trajectory id of given TrajectorySpecifier.
     * @param specifier TrajectorySpecifier describing what trajectory to get the id of.
     * @return The internal database id of the Trajectory specified by specifier.
     * @throws DatabaseException if the id could not be retrieved or if the described Trajectory does not exist in the database.
     */
    private int getTrajectoryId(TrajectorySpecifier specifier) throws DatabaseException {
        if(specifier.getInternalId() != 0){
            return specifier.getInternalId();
        }
        //query database
        try {
            ResultSet results = statements.executeStatement("get_trajectory_id",
                    specifier.getSubject().getId(), specifier.getArm().toString(), specifier.getSensor().toString());
            if(results.next()){
                return results.getInt("id");
            }
            throw new DatabaseException("No such trajectory");
        } catch (SQLException | DatabaseException e) {
            throw new DatabaseException("Could not get trajectoryId: " + e.getMessage(), e);
        }
    }

    /**
     * gets an overview of the available trajectories
     * @return a map linking a subject to the specifiers of all trajectories for that subject, ordered by arm and sensor.
     * @throws DatabaseException if the specifiers could not be retrieved.
     */
    @Override
    public Map<Subject, SpecifierGroup> getAvailableTrajectorySpecifiers() throws DatabaseException {
        Map<Subject, SpecifierGroup> specifiers = new HashMap<>();
        try {
            //SELECT ... FROM subjects s JOIN trajectories t ON t.subject = s.id ORDER BY subject, arm, sensor;
            ResultSet results = statements.executeStatement("getAvailableTrajectorySpecifiers");
            //loop through ResultSet, creating all subjects exactly once
            int prevSubjectId = -1;
            Subject subject = null;
            while (results.next()) {
                if (results.getInt("subject") != prevSubjectId) {
                    //create new subject
                    subject = new Subject(
                            results.getInt("subject"), results.getInt("age"),
                            Gender.valueOf(results.getString("gender")),
                            ExperimentalGroup.valueOf(results.getString("experimental_group")),
                            results.getString("specific_condition")
                    );
                    //add new entry to specifiers
                    specifiers.put(subject, new SpecifierGroup());
                    //update prevSubjectId
                    prevSubjectId = subject.getId();
                }
                //add new TrajectorySpecifier to the map entry of the current subject.
                specifiers.get(subject).add(new TrajectorySpecifier(
                        subject,
                        Arm.valueOf(results.getString("arm")),
                        Sensor.valueOf(results.getString("sensor")),
                        results.getInt("t.id")));
            }
            return specifiers;
        } catch (SQLException e) {
            throw new DatabaseException("could not get available trajectories: " + e.getMessage(), e);
        }
    }

    /**
     * saves given Trajectory to the database.
     *
     * @param trajectory Trajectory to save
     * @throws DatabaseException if the Trajectory could not be saved.
     */
    @Override
    public void saveTrajectory(Trajectory trajectory) throws DatabaseException {
        TrajectorySpecifier specifier = trajectory.getSpecifier();
        Subject subject = specifier.getSubject();
        //create new subject entry if necessary
        if (!getSubjectIds().contains(subject.getId())) {
            saveSubject(subject);
        }
        //save trajectory
        try {
            statements.executeStatement("add_trajectory",
                    subject.getId(),
                    specifier.getArm().toString(),
                    specifier.getSensor().toString());
        } catch (SQLException e) {
            throw new DatabaseException("could not save trajectory: " + e.getMessage(), e);
        }
        //save points
        int trajectoryId = getLastInsertedId();
        for (Point point : trajectory.getPoints()) {
            savePoint(trajectoryId, point);
        }
    }

    /**
     * Deletes a trajectory with a given trajectoryId
     *
     * @param trajectoryId id of trajectory to delete.
     * @throws DatabaseException if given trajectory could not be deleted.
     */
    public void deleteTrajectory(int trajectoryId) throws DatabaseException {
        try {
            deletePoints(trajectoryId);
            statements.executeStatement("delete_trajectory", trajectoryId);
        } catch (SQLException e) {
            throw new DatabaseException("Could not delete trajectory" + e.getMessage(), e);
        }
    }

    /**
     * saves a Point to the database.
     *
     * @param trajectoryId database id of the trajectory to which the Point belongs.
     * @param point        The Point to save.
     * @throws DatabaseException if the Point could not be saved.
     */
    private void savePoint(int trajectoryId, Point point) throws DatabaseException {
        try {
            statements.executeStatement("add_point",
                    trajectoryId, point.getTimestamp(), point.getXpos(), point.getYpos(), point.getZpos());
        } catch (SQLException e) {
            throw new DatabaseException("Could not save point: " + e.getMessage(), e);
        }
    }

    /**
     * Deletes points from a given trajectoryId
     *
     * @param trajectoryID Id of trajectory to delete all points from
     * @throws DatabaseException if the points could not be removed(no connection, no such trajectory)
     */
    private void deletePoints(int trajectoryID) throws DatabaseException{
        try{
            statements.executeStatement("delete_points",
                    trajectoryID);
        } catch (SQLException e) {
            throw new DatabaseException("Could not delete points from: " + e.getMessage(), e);
        }
    }

    /**
     * @return the last auto_increment id inserted into the database.
     * @throws DatabaseException if the last inserted id could not be retrieved.
     */
    private int getLastInsertedId() throws DatabaseException {
        try {
            ResultSet result = statements.executeStatement("get_last_inserted_id");
            result.next();
            return result.getInt(1);
        } catch (SQLException e) {
            throw new DatabaseException("Could not get last inserted id: " + e.getMessage(), e);
        }

    }

    /**
     * save given subject to the database
     *
     * @param subject Subject instance to save
     * @throws DatabaseException if the subject could not be saved.
     */
    private void saveSubject(Subject subject) throws DatabaseException {
        try {
            statements.executeStatement("add_subject",
                    subject.getId(),
                    subject.getAge(),
                    subject.getGender().toString(),
                    subject.getExperimentalGroup().toString(),
                    subject.getSpecificCondition());
        } catch (SQLException e) {
            throw new DatabaseException("Could not save subject: " + e.getMessage(), e);
        }
    }

    /**
     * @return Set containing the ids(int) of the subjects in the databse.
     * @throws DatabaseException if the id's could not be retrieved due to database problems.
     */
    private Set getSubjectIds() throws DatabaseException {
        Set<Integer> ids = new HashSet<>();
        try {
            ResultSet results = statements.executeStatement("get_subject_ids");
            while (results.next()) {
                ids.add(results.getInt("id"));
            }
        } catch (SQLException e) {
            throw new DatabaseException("Could not get existing subject id's: " + e.getMessage(), e);
        }
        return ids;
    }

    /**
     * Deletes subject with given id fron the database, together with all associated data.
     *
     * @param subjectId id of subject to remove
     * @throws DatabaseException if the subject was not found or could not be removed
     */
    public void deleteSubject(int subjectId) throws DatabaseException {
        try {
            statements.executeStatement("delete_points_of_subject", subjectId);
            statements.executeStatement("delete_trajectories", subjectId);
            statements.executeStatement("delete_subject", subjectId);
        }catch (SQLException e){
            throw new DatabaseException("Could not delete subject with id:" + e.getMessage(), e);
        }
    }

}
