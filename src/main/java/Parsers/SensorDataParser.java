package Parsers;


import exceptions.ParseException;
import jdk.nashorn.internal.runtime.ParserException;
import model.Point;
import model.Sensor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * class that parses a datafile. a datafile should be named either of the below:
 •	"greenCoords.dat"(wrist rotation)
 •	“redCoords.dat”(hand location)
 •	“orangeCoords.dat”(upper arm location)
 •	“purpleCoords.dat”(elbow rotation)
 •	“brownCoords.dat”(forearm location)
 All files should be tab-separated, without headers, in the following format:
 "
 12:13:43	-0,56814	-2,04191	0,600106
 12:13:43	-0,56930	-2,02545	0,593495
 12:13:43	-0,57028	-2,02617	0,592252
 ...
 "
 */
public class SensorDataParser {
    private static String timestamp = "\\d{2}:\\d{2}:\\d{2}";
    private static String measurement = "-?\\d+,\\d+";
    private static Pattern linePattern = Pattern.compile(
            String.join("\\t", timestamp, measurement, measurement, measurement));

    private final String filename;
    private final InputStream fileContent;
    private final Sensor sensor;
    private ArrayList<Point> points = new ArrayList<>();


    /**
     * constructor
     * @param filename name of file to parse
     * @param fileContent inputstream on the content of the file
     * @throws ParseException if the file could not be parsed.
     */
    public SensorDataParser(String filename, InputStream fileContent) throws ParseException {
        //validate filename
        this.sensor = getSensorFromFilename(filename);
        this.filename = filename;
        this.fileContent = fileContent;
        parse();
    }

    public String getFilename() {
        return filename;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    /**
     * parses the file linked to this.
     * @throws ParseException if the file could not be read or if the file contains an invalid line.
     */
    private void parse() throws ParseException {
        String line;
        int timestamp = 0;

        //try with; for auto-closing
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(fileContent))){
            //loop through lines, timestamp is not read but generated instead.
            while((line = reader.readLine()) != null){
                //validate line
                if(!linePattern.matcher(line).matches()){
                    throw new ParserException(String.format("invalid line in %s: '%s'.", this.filename, line));
                }
                String[] splitLine = line.replace(",", ".").split("\\t");
                points.add(new Point(timestamp++,
                        Double.parseDouble(splitLine[1]),
                        Double.parseDouble(splitLine[2]),
                        Double.parseDouble(splitLine[3])));
            }
        } catch (IOException e) {
            throw new ParseException("could not read file: " + filename + " " + e.getMessage());
        }
    }

    /**
     * determines the sensor from which this file originates based on given filename.
     * @param filename filename to check.
     * @return sensor from which this file originates based on given filename.
     * @throws ParseException if the file has an invalid name.
     */
    private Sensor getSensorFromFilename(String filename) throws ParseException {
        switch (filename) {
            case "greenCoords.dat":
                return Sensor.WRIST_ROTATION;
            case "redCoords.dat":
                return Sensor.HAND_LOCATION;
            case "orangeCoords.dat":
                return Sensor.UPPER_ARM_LOCATION;
            case "purpleCoords.dat":
                return Sensor.ELBOW_ROTATION;
            case "brownCoords.dat":
                return Sensor.FOREARM_LOCATION;
            default:
                throw new ParseException("You have given a wrong file. You should have given greenCoords.dat, " +
                        "redCoords.dat, orangeCoords.dat, purpleCoords.dat and brownCoords.dat files");
        }
    }

}
