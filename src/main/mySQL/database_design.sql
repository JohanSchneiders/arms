/*
scripts clears the existing database if it exists, and sets it up from scratch.
all other code assumes this database schema.
 */
-- drop tables
DROP TABLE IF EXISTS points;
DROP TABLE IF EXISTS trajectories;
DROP TABLE IF EXISTS subjects;


CREATE TABLE subjects(
  id INT,
  age INT NULL,
  gender ENUM('MALE', 'FEMALE', 'UNKNOWN') NULL,
  experimental_group ENUM('CONTROL', 'PATIENT', 'UNKNOWN') NOT NULL,
  specific_condition VARCHAR(50) NULL,

  PRIMARY KEY(id)
);

CREATE TABLE trajectories(
  -- a trajectory contains the points in space-time of a single sensor.
  id INT AUTO_INCREMENT NOT NULL ,
  subject INT NOT NULL,
  arm ENUM('LEFT', 'RIGHT') NOT NULL,
  sensor ENUM('FOREARM_LOCATION', 'ELBOW_ROTATION', 'UPPER_ARM_LOCATION', 'WRIST_ROTATION', 'HAND_LOCATION') NOT NULL,

  PRIMARY KEY(id),
  FOREIGN KEY(subject) REFERENCES subjects(id),
  CONSTRAINT unique_trajectory UNIQUE(subject, arm, sensor)
);

CREATE TABLE points(
  trajectory INT NOT NULL ,
  time INT NOT NULL ,
  xpos DOUBLE NOT NULL ,
  ypos DOUBLE NOT NULL ,
  zpos DOUBLE NOT NULL ,

  PRIMARY KEY (trajectory, time) ,
  FOREIGN KEY (trajectory) REFERENCES trajectories(id)
);

-- stored procedures?
